.. pyts2 documentation master file, created by
   sphinx-quickstart on Tue Dec  8 13:40:43 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyts2's documentation!
=================================

The second generation TimeStream image processing library in Python (3),
incorporating the TimeStreamToolKit (``tstk``) command line interface.

.. image:: https://anaconda.org/appfanu/pyts2/badges/version.svg   
   :target: https://conda.anaconda.org/appfanu
.. image:: https://anaconda.org/appfanu/pyts2/badges/latest_release_date.svg
   :target: https://conda.anaconda.org/appfanu
.. image:: https://anaconda.org/appfanu/pyts2/badges/platforms.svg
   :target: https://conda.anaconda.org/appfanu
.. image:: https://anaconda.org/appfanu/pyts2/badges/license.svg
   :target: https://conda.anaconda.org/appfanu
.. image:: https://anaconda.org/appfanu/pyts2/badges/downloads.svg
   :target: https://conda.anaconda.org/appfanu
.. image:: https://anaconda.org/appfanu/pyts2/badges/installer/conda.svg
   :target: https://conda.anaconda.org/appfanu

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api
   cli
   pipeline
   timestreams
   glossary


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
