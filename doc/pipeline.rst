.. currentmodule:: pyts2.pipeline

Pipeline Reference
------------------

Align time
==========

.. automodule:: pyts2.pipeline.align_time
   :members:

Audit
=====

.. automodule:: pyts2.pipeline.audit
   :members:

Base
====

.. automodule:: pyts2.pipeline.base
   :members:
   :inherited-members:


Imageio
=======

.. automodule:: pyts2.pipeline.imageio
  :members:

Resize
======

.. automodule:: pyts2.pipeline.resize
   :members:

Gigavision
======

.. automodule:: pyts2.pipeline.gigavision
  :members:
