#!/bin/bash
export $(cat .env | xargs)

RESOURCE_GROUP_NAME=${RESOURCE_GROUP_NAME:-${APP_NAME}-rg}
STORAGE_NAME=${STORAGE_NAME:-${APP_NAME}stg}

set -xe

# create resource group
az group create \
    --name "${RESOURCE_GROUP_NAME}" \
    --location "${LOCATION}"

# create storage account
az storage account create \
    --name $(echo "${STORAGE_NAME}" | tr '[:upper:]' '[:lower:]') \
    --resource-group "${RESOURCE_GROUP_NAME}" \
    --location "${LOCATION}" \
    --sku Standard_LRS

az functionapp create \
    --name "${APP_NAME}" \
    --resource-group "${RESOURCE_GROUP_NAME}" \
    --storage-account "${STORAGE_NAME}" \
    --consumption-plan-location "${LOCATION}" \
    --runtime python \
    --runtime-version 3.8 \
    --functions-version 3 \
     --os-type linux