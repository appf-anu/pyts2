#!/bin/bash
set -e
set -o pipefail
export $(cat .env | xargs)
TEST_CAMERA_NAME=test_camera
usage(){
    echo "usage ./test.sh [-p] [-d] [-s] [-h] your_test_image.jpg"
}

MODE=local
ENDPOINT=http
IMAGE_FILE=${@: -1}
while getopts 'pshd' OPTION; do
    case "$OPTION" in
        p)
            
            MODE='prod'  
            INVOKE_URL="https://${APP_NAME}.azurewebsites.net/api/http"
            ;;
        s)
            TEST_STORAGE=true

            ;;

        d)
            ENDPOINT=deprecated
            ;;
        ?|h)
            usage
            exit 1
            ;;
    esac
done

if [[ -z $1 ]]; then
    echo "no image provided"
    usage
    exit 1
fi
if [[ ! -f "$IMAGE_FILE" ]]; then
    echo "image file must exist"
    exit 1
fi

INVOKE_URL="http://localhost:7071/api/${ENDPOINT}"

if [[ "$MODE" == "prod" ]]; then
    INVOKE_URL="https://${APP_NAME}.azurewebsites.net/api/${ENDPOINT}"
fi


if [[ ! -z $TEST_STORAGE ]]; then
    echo "[$MODE] measurement and storage" 1>&2
    if ! command -v jq &> /dev/null; then
        curl -s -D /dev/stderr -X POST -H "x-functions-key: ${APP_KEY}" \
            "${INVOKE_URL}?discard=false&full_return=true&pretty_json=true&camera_name=$TEST_CAMERA_NAME" -F "image=@$IMAGE_FILE"
    else
        curl -s -D /dev/stderr -X POST -H "x-functions-key: ${APP_KEY}" \
            "${INVOKE_URL}?discard=false&full_return=true&camera_name=$TEST_CAMERA_NAME" -F "image=@$IMAGE_FILE"|jq    
        fi
else
    echo "[$MODE] measurement" 1>&2
    if ! command -v jq &> /dev/null; then
        curl -s -D /dev/stderr -X POST -H "x-functions-key: ${APP_KEY}" \
            "${INVOKE_URL}?discard=true&full_return=true&camera_name=$TEST_CAMERA_NAME" -F "image=@$IMAGE_FILE"
        else
            curl -s -D /dev/stderr -X POST -H "x-functions-key: ${APP_KEY}" \
            "${INVOKE_URL}?discard=true&full_return=true&pretty_json=true&camera_name=$TEST_CAMERA_NAME" -F "image=@$IMAGE_FILE"|jq
        fi
fi