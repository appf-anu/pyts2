import azure.functions as func
import time
import gc
import json
import re
import os
import logging
import datetime
from io import BytesIO
from base64 import b64decode
# pyts2 modules
from pyts2 import pipeline
from pyts2.time import TSInstant
from pyts2 import InMemoryTimeStream, TimestreamFile

# for parsing X-Data-Timestamp and modifying the timezone
from dateutil.parser import parse as dateutil_parse
import pytz

# b2 stuff
from b2sdk.v1 import InMemoryAccountInfo, B2Api


def round_floats(o):
    if isinstance(o, float):
        return round(o, 4)
    if isinstance(o, dict):
        return {k: round_floats(v) for k, v in o.items()}
    if isinstance(o, (list, tuple)):
        return [round_floats(x) for x in o]
    return o


b2_application_key_id = os.environ.get("B2_APPLICATION_KEY_ID")
b2_application_key = os.environ.get("B2_APPLICATION_KEY")
b2_bucket_name = os.environ.get("B2_BUCKET_NAME")

influxdb_url = os.environ.get("INFLUXDB_URL")
# You can generate a Token from the "Tokens Tab" in the UI
# tokens for influxdb1.8 are "username:password"
influxdb_token = os.environ.get("INFLUXDB_TOKEN")
if not influxdb_token:
    influxdb_token = f'{os.environ.get("INFLUXDB_USER")}:{os.environ.get("INFLUXDB_PASSWORD")}'
influxdb_org = os.environ.get("INFLUXDB_ORG")
# bucket for influxdb1.8 will be "database/rp" or just "database"
influxdb_bucket = os.environ.get("INFLUXDB_BUCKET", os.environ.get("INFLUXDB_DATABASE"))

header_parser_re = re.compile(r'.*(?P<camera_name>[A-Z]{4}.[A-Z]{3}.C[0-9][0-9])-(?P<filename>.*.JPG)')


tz = pytz.timezone(os.environ.get("TSTK_TZ", "Australia/Brisbane"))
tzinfos = {pytz.timezone(x).localize(datetime.datetime.now(), is_dst=False).tzname(): x for x in pytz.common_timezones}


def main(req: func.HttpRequest) -> func.HttpResponse:  # these metrics are defined over a typical 8-bit RGB image
    t = time.time()
    parameters = dict(req.params)
    # check if a file has been posted in a form.
    f = req.files.get("image")
    if not f:
        # check if file is posted as a raw http request.
        # this is what axis cameras do.
        ctype = req.headers.get("content-type")
        if ctype in {'image/png', 'image/jpeg'}:
            f = BytesIO(req.get_body())
        elif 'application/json' in ctype:
            # this is what happens with a logic app that sets the x-subject header.
            try:

                databody = req.get_json()
                if req.headers.get("x-subject"):
                    m = header_parser_re.search(req.headers['x-subject'])
                    if m:
                        # the cameras replace underscores with spaces in the subject
                        cname = m.group("camera_name").replace(" ", "_")
                        parameters["camera_name"] = cname
                    else:
                        logging.error("Couldn't parse camera name from subject header")

                if "ContentBytes" in databody.keys():
                    f = BytesIO(b64decode(databody.get("ContentBytes")))
                elif "body" in databody.keys() and "ContentBytes" in databody['body'].keys():
                    f = BytesIO(b64decode(databody['body'].get("ContentBytes")))
                else:
                    return func.HttpResponse(f"invalid json message, no ContentBytes or body",
                                             status_code=400)
            except Exception as e:
                return func.HttpResponse(f"exception trying to decode json message: {str(e)}",
                                         status_code=400)
        else:
            return func.HttpResponse(f"no files or invalid Content-Type ({ctype})",
                                     status_code=400)

    instant = datetime.datetime.now(tz)
    if req.headers.get("X-Data-Timestamp", req.headers.get("x-data-timestamp")):
        try:
            instant = dateutil_parse(req.headers.get("X-Data-Timestamp", req.headers.get("x-data-timestamp")),
                                     tzinfos=tzinfos,
                                     dayfirst=True)
            if instant.tzinfo is None:
                instant = instant.localize(tz)
        except Exception as e:
            return func.HttpResponse(str(e), status_code=400)

    # do default parameters
    for env_k, env_v in os.environ.items():
        if not env_k.startswith("DEFAULT_PARAM_"):
            continue
        param_k = env_k.lstrip("DEFAULT_PARAM_").lower()
        parameters[param_k] = parameters.get(param_k, env_v)

    min_mean_luminance = parameters.get("min_mean_luminance")
    if min_mean_luminance is None:
        min_mean_luminance = "-inf"
    try:
        min_mean_luminance = float(min_mean_luminance)
    except ValueError:
        min_mean_luminance = float("-inf")

    # make sure camera name exists, otherwise fail.
    parameters['camera_name'] = parameters.get('camera_name', parameters.get('camera'))
    if not parameters.get('camera_name'):
        return func.HttpResponse(body=json.dumps({
            "error": "camera_name or name parameter must be set"
        }), status_code=400)
    # set tag for Camera
    parameters['tag_Camera'] = parameters['camera_name']

    # make sure org_name exists, fill default org
    parameters['org_name'] = parameters.get('org_name', parameters.get('org', 'default_org'))
    # set tag for Organsation
    parameters['tag_Organisation'] = parameters['org_name']
    json_opts = dict()
    if parameters.get("pretty_json") not in {"false", "no", "0", 0, None, False}:
        json_opts['indent'] = 4
        json_opts['sort_keys'] = True

    tags = {k.lstrip("tag_"): v for k, v in parameters.items() if k.startswith("tag_")}

    # make sure to rewind the file buffer
    f.seek(0)
    # create in memory timestream to process pipeline
    ints = InMemoryTimeStream(name=parameters['camera_name'])

    ints.write(TimestreamFile.from_bytes(f.read(), "", instant=TSInstant(instant)))
    del f
    gc.collect()
    init_time = time.time() - t
    t = time.time()
    pipe = pipeline.TSPipeline(record_step_times=True)
    pipe.add_step(pipeline.TruncateTimeStep(truncate_to=parameters.get('truncate_time_to', 10)))

    metrics_pipeline = pipeline.TSPipeline(
        pipeline.TimeStreamPathRenameStep(name=parameters['camera_name']),
        pipeline.FileStatsStep(),
        pipeline.DecodeImageFileStep(),
        pipeline.PhenocamRGBMetricStep(),
        pipeline.ImageMeanColourStep(),
        pipeline.CalculateEVStep(),
        pipeline.ClearFileObjectStep(),
        pipeline.InfluxClientRecordStep(
            "image_metrics",
            influxdb_url,
            influxdb_bucket,
            influxdb_token,
            tags=tags,
            influxdb_org=influxdb_org
        ),
        record_step_times=True,
        step_id="metrics"
    )

    # this TeeStep needs to update the report for
    pipe.add_step(pipeline.TeeStep(metrics_pipeline, update_report=True))

    if parameters.get("discard") in {"false", "no", "0", 0, None, False}:
        # begin file storage block

        # todo: write a backend to store the auth token for this so we arent
        # reauthorizing for every image
        info = InMemoryAccountInfo()
        b2_api = B2Api(info)
        b2_api.authorize_account("production",
                                 b2_application_key_id,
                                 b2_application_key)
        # decode and re-encode for non-jpg images
        pipe.add_step(pipeline.ConditionalStep(
            lambda img: img.format != "jpg",
            pipeline.TSPipeline(
                pipeline.DecodeImageFileStep(),
                pipeline.EncodeImageFileStep(format="jpg"),
                record_step_times=True,
                step_id="jpg_encode"
            )
        ))

        # set values for output file paths.
        orig_name = f"{parameters['camera_name']}~orig"
        orig_prefix = f"cameras/{parameters['org_name']}/{parameters['camera_name']}/{orig_name}"
        downsize_name = f"{parameters['camera_name']}~720x"
        downsize_prefix = f"cameras/{parameters['org_name']}/{parameters['camera_name']}/{downsize_name}"

        # ouptut pipeline
        pipe.add_step(pipeline.ConditionalStep(
            lambda img: img.report["ImageMean_L"] >= min_mean_luminance,
            pipeline.TSPipeline(
                pipeline.TimeStreamPathRenameStep(name=orig_name),
                pipeline.WriteToB2Step(b2_api=b2_api,
                                       prefix=orig_prefix),
                pipeline.ResizeImageStepPIL(geom="720x", interpolation="nearest"),
                pipeline.TimeStreamPathRenameStep(name=downsize_name),
                pipeline.WriteToB2Step(b2_api=b2_api,
                                       prefix=downsize_prefix),
                record_step_times=True,
                step_id="resize_store"
            )
        ))
        # end file storage

    # clear the file storage at the end, probably doesnt do anything but oh well.
    pipe.add_step(pipeline.ClearFileObjectStep())

    # return the
    if parameters.get("full_return") not in {"false", "no", "0", 0, None, False}:
        images = []
        tstart = time.time()
        for image in pipe.process(ints, ncpus=1):
            images.append(image.dict())
        processing_time = time.time() - tstart
        return func.HttpResponse(json.dumps({"results": round_floats(images),
                                             "processing_time": processing_time,
                                             "init_time": init_time
                                             }, **json_opts)
                                 )
    ncomp = 0
    tstart = time.time()
    for _ in pipe.process(ints, ncpus=1):
        ncomp += 1
    processing_time = time.time() - tstart
    return func.HttpResponse(json.dumps({"completed": ncomp,
                                         "processing_time": processing_time,
                                         "init_time": init_time
                                         }, **json_opts))
