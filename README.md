# pyts2: Next-generation TimeStream utility

[![Documentation Status](https://readthedocs.org/projects/pyts2/badge/?version=latest)](https://pyts2.readthedocs.io/en/latest/?badge=latest)
[![Anaconda-Server Badge](https://anaconda.org/appfanu/pyts2/badges/version.svg)](https://anaconda.org/appfanu/pyts2)
[![Anaconda-Server Badge](https://anaconda.org/appfanu/pyts2/badges/latest_release_relative_date.svg)](https://anaconda.org/appfanu/pyts2)
[![Anaconda-Server Badge](https://anaconda.org/appfanu/pyts2/badges/platforms.svg)](https://anaconda.org/appfanu/pyts2)
[![Anaconda-Server Badge](https://anaconda.org/appfanu/pyts2/badges/license.svg)](https://anaconda.org/appfanu/pyts2)
[![Anaconda-Server Badge](https://anaconda.org/appfanu/pyts2/badges/downloads.svg)](https://anaconda.org/appfanu/pyts2)
[![Anaconda-Server Badge](https://anaconda.org/appfanu/pyts2/badges/installer/conda.svg)](https://conda.anaconda.org/appfanu)

A Python3 library and CLI for manipulating time-series image data, with a focus
on plant phenomics.

Created by the ANU node of the Australian Plant Phenomics Facility (principally
by Kevin Murray).

## License

Licensed under the Mozilla Public License 2.0. In short this license means you
can use this code as a python library however you like (including
commercially), but if you modify the code of pyts2, please release your
improvements to our code under the same license.
